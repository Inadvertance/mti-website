'use strict';

$(document).ready(() => {
  // Defines material design timing function.
  $.extend($.easing, {
    // t: current time, b: start value, c: end value, d: duration
    materialSwiftOut: function (x, t, b, c, d) {
      let ts = (t /= d) * t;
      let tc = ts * t;
      return b + c * (-6 * tc * ts + 13 * ts * ts + -10 * tc + 4 * ts);
    }
  });

  $('.mti-navbar__link > a[href^="#"]').on('click', function() {
    var page = $(this).attr('href');

    $('html, body').animate({
      scrollTop: $(page).offset().top
    }, 600, 'materialSwiftOut');
    return false;
  });
});
